
require('./bootstrap');
require('./sb-admin-2');
require( '@fortawesome/fontawesome-free' );

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const app = new Vue({
    el: '#app',
});
